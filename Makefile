## Makefile --- ematrix Makefile.             -*- lexical-binding: t; -*-

# Copyright (C) 2024  Pierre-Antoine Rouby

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

GZ = gzip

EMACS = emacs -Q -q --batch -nw
EMACS_COMPILE = -f emacs-lisp-byte-compile
EMACS_NATIVE_COMPILE = -f emacs-lisp-native-compile
EMACS_DIR = ~/.emacs.d/matrix/

.PHONY: clean install compile

all: compile

compile: matrix.elc matrix.eln matrix.el.gz

%.el.gz: %.el
	$(info GZ           $@)
	@$(GZ) -k $<

%.elc: %.el
	$(info BYTE         $@)
	@$(EMACS) $< $(EMACS_COMPILE)

%.eln: %.el
	$(info NATIVE       $@)
	@$(EMACS) $< $(EMACS_NATIVE_COMPILE)

install: compile
	$(info INSTALL)
	@mkdir -p $(EMACS_DIR)
	@cp -v *.el.gz  $(EMACS_DIR)
	@cp -v *.elc $(EMACS_DIR)
	@cp -v *.eln $(EMACS_DIR)

clean:
	$(info CLEAN)
	@rm -v *.elc
	@rm -v *.eln
	@rm -v *.el.gz
